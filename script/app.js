// const url = "https://iotapi.tutofree.fr/";
const url = "http://localhost:3000/";
const rooms_container = document.getElementById("rooms_container");
const data = {
    token: "",
    data: []
};

window.onload = async function () {

    if (rooms_container) {
        //Login
        await fetchToken(
            'login',
            {
                "email": "johndoe@domain.ext",
                "password": "123456"
            }
        );

        // Fetch Data
        await fetchData('rooms');
    }
}

/**
 * Function calls API in the order to fetch data
 * @param {*} uri_root 
 * @param {*} data 
 */
async function fetchData(uri_root, params = {}) {
    const response = axios({
        method: 'GET',
        url: `${url}${uri_root}`,
        headers: {
            'Authorization': `bearer ${data.token}`
        }
    }).then(function (response) {
        data.data = response?.data?.data;
        data.data.map(room => {
            createRoom(room);
        });
    }).catch(function (error) {
        console.log(error);
    });
}

/**
 * Function for Login user in the order to feacth token
 * @param {*} uri_root 
 * @param {*} user_data 
 */
async function fetchToken(uri_root, user_data = {}) {
    const response = await axios({
        method: 'POST',
        url: `${url}${uri_root}`,
        data: user_data
    });

    if (response.status == 200) {
        data.token = response?.data?.token
    }

}

function showData() {

    if (rooms_container) {
        data.data.map(room => {
            createRoom(room);
        });
    }
}

function rssiToMeter(RSSI) {
    const Measured_Power = -69;
    const N = 2.0;
    return Math.pow(10, ((Measured_Power - (RSSI)) / (10 * N))).toFixed(2);
}

function createRoom(data) {
    const div = document.createElement('div');
    div.classList.add('col', 'col-md-4', 'col-sm-6', 'col-12');

    const temperature = `${data.temperature.value}`;
    const humidity = `${data.humidity.value}${data.humidity.unit}`;
    let positions = ""

    data.positions.map(position => {
        positions += `<div>
            <p>${position.tag_name}</p>
            <p>${rssiToMeter(data.positions[0]?.tag_rssi)}m</p>
        </div>`;
    });

    const icons = {
        temperature: {
            cool: 'https://img.icons8.com/external-smashingstocks-flat-smashing-stocks/66/000000/external-freezing-weather-smashingstocks-flat-smashing-stocks.png',
            hot: 'https://img.icons8.com/cute-clipart/64/000000/sun--v2.png',
            warm: 'https://img.icons8.com/external-smashingstocks-flat-smashing-stocks/66/000000/external-hot-weather-weather-smashingstocks-flat-smashing-stocks.png',
        },
        humidity: {
            dry: 'https://img.icons8.com/cute-clipart/64/000000/dry.png',
            normal: 'https://img.icons8.com/external-smashingstocks-flat-smashing-stocks/66/000000/external-humidity-weather-smashingstocks-flat-smashing-stocks.png',
            wet: 'https://img.icons8.com/external-smashingstocks-flat-smashing-stocks/66/000000/external-humidity-weather-smashingstocks-flat-smashing-stocks-2.png'
        },
        positions: {
            close: 'https://img.icons8.com/external-smashingstocks-flat-smashing-stocks/66/000000/external-map-location-education-smashingstocks-flat-smashing-stocks.png',
            far: 'https://img.icons8.com/cute-clipart/64/000000/door-closed.png'
        }
    };


    div.innerHTML = `
        <div class="card p-3 m-2">
            <span class="icon">
            <img class="img-fluid" src="https://img.icons8.com/cute-clipart/64/000000/sun.png" />
            </span>
            <div class="title">
                <p>${data.roomId}</p>
            </div>
            <div class="row">

                <div class="col-4 humidity">
                    <div class="icon">
                        <img class="img-fluid" src="${icons.humidity.normal}"/>
                    </div>
                    <div class="header">Humidity</div>
                    <div class="value">${parseFloat(humidity).toFixed(0)}${data.humidity.unit}</div>
                </div>

                <div class="col-4 temperature">
                    <div class="icon">
                        <img class="img-fluid" src="${temperature < 0 ? icons.temperature.cool : icons.temperature.warm}"/>
                    </div>
                    <div class="header">Temperature</div>
                    <div class="value">${parseFloat(temperature).toFixed(0)}${data.temperature.unit}</div>
                </div>
                <div class="col-4 positions">
                    <div class="icon">
                        <img class="img-fluid" src="${icons.positions.close}"/>
                    </div>
                    <div class="header">Position</div>
                    <div class="value">${positions}</div>
                </div>
            </div>

        </div>
    `;

    rooms_container.appendChild(div);
}